# Git@OSC 代码仓库 Web 嵌入部件


`Git-OSC_RW.html` 是基于 **Git@OSC 代码仓库挂件**官方代码（`http://git.oschina.net/{UID}/{Repository}/widget`）优化而来的 **非官方开源封装版**，主要有以下特点 ——

 1. UI 样式更大方、响应式
 2. 独立代码文件，方便 **工程复用**
 3. 通过 `<iframe />` 引用，方便 **异步、延迟加载**（同域下 **自适应内容高度**）
 4. 支持 Commit、Issue 隐藏
 5. 超链接在新窗口打开


## 【使用方法】

### （一）异步加载

```HTML
<iframe frameborder="0" allowtransparency="true"
    src="path/to/Git-OSC_RW.html?repository={UID}/{Repository}&hidden=issue">
</iframe>
```
（上述代码也隐藏了 Issue 动态）

### （二）延迟加载

以 [EasyImport.js](http://git.oschina.net/Tech_Query/EasyImport.js) 内置的 **延迟加载**特性为例 ——

```HTML
<iframe frameborder="0" allowtransparency="true"
    data-src="path/to/Git-OSC_RW.html?repository={UID}/{Repository}&hidden=commit,issue">
</iframe>
```
（上述代码也隐藏了 Commit、Issue 动态）


## 【项目资料】

 - 开发初衷 —— http://git.oschina.net/oschina/git-osc/issues/4974
 - 主要版本 —— http://git.oschina.net/Tech_Query/Git-OSC_Repository-Widget/releases
 - 演示页面 —— http://tech_query.oschina.io/git-osc_repository-widget/Git-OSC_RW.html